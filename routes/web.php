<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@Index');

Route::post('/loginValidate', 'HomeController@login');
Route::get('/loginValidate', 'HomeController@login');

Route::get('/ExportarWord', 'ReporteController@ExportarWord');
Route::get('/DescargaArchivo/{file_name}', 'ReporteController@DescargaArchivo');
Route::get('/ExportarExcel', 'ReporteController@ExportarExcel');

Route::post('/ObtenerEstado', 'HomeController@ObtenerEstado');
Route::get('/ObtenerEstado', 'HomeController@ObtenerEstado');

Route::post('/ObtenerCondado', 'HomeController@ObtenerCondado');
Route::get('/ObtenerCondado', 'HomeController@ObtenerCondado');

Route::post('/ObtenerMexicanos', 'HomeController@ObtenerMexicanos');
Route::get('/ObtenerMexicanos', 'HomeController@ObtenerMexicanos');

Route::post('/ObtenerTotalRegistros', 'HomeController@ObtenerTotalRegistros');
Route::get('/ObtenerTotalRegistros', 'HomeController@ObtenerTotalRegistros');

Route::post('/ObtenerTotalViajeros', 'HomeController@ObtenerTotalViajeros');
Route::get('/ObtenerTotalViajeros', 'HomeController@ObtenerTotalViajeros');

Route::get('/logout', 'HomeController@logout');
