<?php

namespace App\Http\Controllers;

use DB;

class ReporteController extends Controller
{

    public function ExportarWord(){
        set_time_limit(300);
        ini_set('memory_limit', '-1');
        session_start();
        $queryCorreo = $_SESSION["queryCorreo"];
        //$queryGroupBy=DB::select("SET @@SQL_MODE = REPLACE(@@SQL_MODE, 'ONLY_FULL_GROUP_BY', '')");
        $correos = DB::connection('mysql')->select($queryCorreo);

        //$correos=DB::select($queryCorreo);//mysqli_query($conexion->conexion(), $queryCorreo);

        // Creating the new document...
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $pais = "";
        $estado = "";
        $correoContacto = "";
        $section = $phpWord->addSection();
        foreach ($correos as $key => $correo) {
          if($pais != $correo->pais){
              $pais = $correo->pais;
              $fontStyle = new \PhpOffice\PhpWord\Style\Font();
              $fontStyle->setBold(true);
              $fontStyle->setName('Calibri');
              $fontStyle->setSize(12);
              $myTextElement = $section->addText(''.$pais.'');
              $myTextElement->setFontStyle($fontStyle);
          }

           if($estado != $correo->estado){

              if($correoContacto != ""){
                  $fontStyle = new \PhpOffice\PhpWord\Style\Font();
                  $fontStyle->setBold(false);
                  $fontStyle->setName('Calibri');
                  $fontStyle->setSize(8);
                  $myTextElement = $section->addText(''.$correoContacto.'');
                  $myTextElement = $section->addText('');
                  $myTextElement->setFontStyle($fontStyle);
                  $correoContacto = "";
              }

              $estado = $correo->estado;
              $fontStyle = new \PhpOffice\PhpWord\Style\Font();
              $fontStyle->setBold(false);
              $fontStyle->setName('Calibri');
              $fontStyle->setSize(10);
              $myTextElement = $section->addText(''.$estado.'');
              $myTextElement->setFontStyle($fontStyle);
          }

          if($correo->contacto != ""){
              $correoContacto .= $correo->contacto."; ";
          }
        }

        if($correoContacto != ""){
            $fontStyle = new \PhpOffice\PhpWord\Style\Font();
            $fontStyle->setBold(false);
            $fontStyle->setName('Calibri');
            $fontStyle->setSize(8);
            $myTextElement = $section->addText(''.$correoContacto.'');
            $myTextElement->setFontStyle($fontStyle);
            $correoContacto = "";
        }


        $format = 'Word2007';

        $mime = array(
            'Word2007'  => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'ODText'    => 'application/vnd.oasis.opendocument.text',
            'RTF'       => 'application/rtf',
            'HTML'      => 'text/html',
            'PDF'       => 'application/pdf',
        );
        $date = date("Y-m-d H:i:s");
        $filename = "SIRME_".$date.".docx";
        $writer = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, $format);
        /*
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . $filename . $date .'.docx"');
        header('Content-Type: ' . $mime[$format]);
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        $filename = 'php://output'; // Change filename to force download
        */
        $save_file_name = public_path("/reporte/".$filename);
        $writer->save($save_file_name);

        return $filename;
    }

    public function DescargaArchivo($file_name){
      if ($file_name){
        $filepath = public_path("/reporte/".$file_name);

        return response()->download($filepath)->deleteFileAfterSend(true);
      }
    }

    public function ExportarExcel(){
        set_time_limit(300);
        ini_set('memory_limit', '-1');
        $date = date("Y-m-d H:i:s");
        $filename = "SIRME_".$date;
        $writer = \Excel::create($filename, function($excel){

            session_start();

            $queryTitulares = $_SESSION["queryListaMexicanos"];
            $fecha=$_SESSION["fecha"];
            $idCase=$_SESSION["idCase"];

            $excel->sheet('SIRME', function($sheet) use ($queryTitulares, $fecha, $idCase){
                //Merge de celdas para titulo
                $sheet->mergeCells('A1:M1');

                //Manipular Celdas
                $sheet->cells('A1:M1', function($cells) {

                    //Colorear Fondo
                    //$cells->setBackground('#D8D8D8');

                    // Set font
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '18',
                        'bold'       =>  true
                    ));

                    //Alineación de Celdas
                    $cells->setAlignment('center');

                    $cells->setValue('Reporte General del Sistema de Registro para Mexicanos en el Exterior');
                });

                $sheet->cell('D3', function($cell) {
                	$cell->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '13',
                        'bold'       =>  true
                    ));

                    $cell->setValue('Fecha del Reporte');
                });

                $sheet->cell('D4', function($cell) use ($fecha){
                    $cell->setValue($fecha);
                });

                $sheet->cell('F3', function($cell) {
                	$cell->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '13',
                        'bold'       =>  true
                    ));

                    $cell->setValue('País');
                });

                if ($idCase > 1 ){
                    $sheet->cell('G3', function($cell) {
                        $cell->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '13',
                            'bold'       =>  true
                        ));

                        $cell->setValue('Estado');
                    });
                }


                //Estadistica
                $sheet->cells('K2:K6', function($cells) {
                        $cells->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '13',
                            'bold'       =>  true
                        ));
                    });
                $sheet->mergeCells('K2:L2');
                $sheet->cell('K2', function($cell) {
                    $cell->setValue('Total Viajeros');
                });

                $sheet->cell('M2', function($cell) {
                    $cell->setValue($_SESSION["totalViajeros"]);
                });


                $sheet->mergeCells('K3:L3');
                $sheet->cell('K3', function($cell) {
                    $cell->setValue('Total Familiares Viajeros');
                });

                $sheet->cell('M3', function($cell) {
                    $cell->setValue($_SESSION["totalViajerosFamiliares"]);
                });

                $sheet->mergeCells('K4:L4');
                $sheet->cell('K4', function($cell) {
                    $cell->setValue('Total Residentes');
                });

                $sheet->cell('M4', function($cell) {
                    $cell->setValue($_SESSION["totalResidentes"]);
                });

                $sheet->mergeCells('K5:L5');
                $sheet->cell('K5', function($cell) {
                    $cell->setValue('Total Familiares Residentes');
                });

                $sheet->cell('M5', function($cell) {
                    $cell->setValue($_SESSION["totalResidentesFamiliares"]);
                });

                $sheet->mergeCells('K6:L6');
                $sheet->cell('K6', function($cell) {
                    $cell->setValue('Total Registros SIRME');
                });

                $sheet->cell('M6', function($cell) {
                    $cell->setValue($_SESSION["totalTitulares"]);
                });


                $sheet->cell('B6', function($cell) {
                	$cell->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '13',
                        'bold'       =>  true
                    ));

                    $cell->setValue('Connacional');
                });

                //Manipular Celdas
                $sheet->cells('A7:M7', function($cells) {

                    //Colorear Fondo
                    $cells->setBackground('#7FB09C');

                    // Set font
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '11',
                        'bold'       =>  true
                    ));

                    //Alineación de Celdas
                    $cells->setAlignment('center');
                });

                $sheet->cell('B7', function($cell) {
                    $cell->setValue('Nombre');
                });

                $sheet->cell('C7', function($cell) {
                    $cell->setValue('Fecha Nacimiento');
                });

                $sheet->cell('D7', function($cell) {
                    $cell->setValue('Sexo');
                });

                $sheet->cell('E7', function($cell) {
                    $cell->setValue('Estancia');
                });

                $sheet->cell('F7', function($cell) {
                    $cell->setValue('Teléfono');
                });

                $sheet->cell('G7', function($cell) {
                    $cell->setValue('Correo/Red Social');
                });

                $sheet->cell('H7', function($cell) {
                    $cell->setValue('País');
                });

                $sheet->cell('I7', function($cell) {
                    $cell->setValue('Estado');
                });

                $sheet->cell('J7', function($cell) {
                    $cell->setValue('Fecha Inicio Viaje');
                });

                $sheet->cell('K7', function($cell) {
                    $cell->setValue('Fecha Fin Viaje');
                });

                $sheet->cell('L7', function($cell) {
                    $cell->setValue('Teléfono Exterior');
                });

                $sheet->cell('M7', function($cell) {
                    $cell->setValue('Dirección');
                });

                //Conexion a la BD

                //$queryGroupBy=DB::select("SET @@SQL_MODE = REPLACE(@@SQL_MODE, 'ONLY_FULL_GROUP_BY', '')");
                $resultado = DB::connection('mysql')->select($queryTitulares);

				        $i=7;
                $titular=0;
                $NombrePais = "";
                $NombreEstado = "";
                $colorFondoTitular = '#E6F0EC';
                foreach ($resultado as $key => $fila) {
                  $titular++;
                  $i++;

                    $sheet->cell('A'.$i, function($cell) use ($titular){
                        $cell->setValue($titular);
                    });

    				$sheet->cell('B'.$i, function($cell) use ($fila){
                        $cell->setValue($fila->fullnombre);
                    });

    				$sheet->cell('C'.$i, function($cell) use ($fila){
                        $cell->setValue($fila->fechaNacimiento);
                    });

                     if($fila->sexo = 1)
                        $sheet->cell('D'.$i, function($cell) use ($fila){
                            $cell->setValue("M");
                        });
                    else
                        $sheet->cell('D'.$i, function($cell) use ($fila){
                            $cell->setValue("F");
                        });

                    $sheet->cell('E'.$i, function($cell) use ($fila){
                        $cell->setValue($fila->Estancia);
                    });

                    $sheet->cell('F'.$i, function($cell) use ($fila){
                        $cell->setValue($fila->Telefono);
                    });

                    $sheet->cell('G'.$i, function($cell) use ($fila){
                        $cell->setValue($fila->CorreoRedSocial);
                    });

                    $sheet->cell('H'.$i, function($cell) use ($fila){
                        $cell->setValue($fila->pais);

                    });

                    if ($NombrePais == ""){
                        $NombrePais = $fila->pais;
                        $sheet->cell('F4', function($cell) use ($NombrePais){
                            $cell->setValue($NombrePais);
                        });
                    }

                    $sheet->cell('I'.$i, function($cell) use ($fila){
                        $cell->setValue($fila->estado);
                    });

                    if ($NombreEstado == "" && $idCase>1){
                        $NombreEstado = $fila->estado;
                        $sheet->cell('G4', function($cell) use ($NombreEstado){
                            $cell->setValue($NombreEstado);
                        });
                    }

                    $sheet->cell('J'.$i, function($cell) use ($fila){
                        $cell->setValue($fila->inicioViaje);
                    });

                    $sheet->cell('K'.$i, function($cell) use ($fila){
                        $cell->setValue($fila->finViaje);
                    });

                    $sheet->cell('L'.$i, function($cell) use ($fila){
                        $cell->setValue(" ".$fila->TelefonoE." ");
                    });

                    $sheet->cell('M'.$i, function($cell) use ($fila){
                        $cell->setValue($fila->Direccion);
                    });

                    if($fila->tieneFamiliar==1){

                        //Ejecuta query para obtner los familiares
                        $queryFamiliares = "SELECT concat(upper(familiar.nombre), ' ', upper(familiar.ap_paterno), '  ', upper(familiar.ap_materno)) as fullnombrefamiliar, date_format(familiar.fec_nacimiento, '%Y/%m/%d') as fechaNacimiento, familiar.sexo, estadofamilia.estado,catalogo_det.descripcion as nacionalidad, (select catalogo_det.descripcion FROM catalogo_det where catalogo_det.id_catalogo = 4 and catalogo_det.id_opcion = viajerofamiliar.id_parentesco)  as parentesco from persona inner join viajero on viajero.id_persona = persona.id_persona inner join viajerofamiliar on viajerofamiliar.id_viajero = viajero.id_viajero inner join persona familiar on familiar.id_persona = viajerofamiliar.id_persona inner join cat_estado estadofamilia on estadofamilia.id_estado = familiar.id_estado inner join catalogo_det on catalogo_det.id_catalogo = 7 and catalogo_det.id_opcion = viajerofamiliar.nacionalidad where persona.id_persona = ".$fila->id_persona;

                        $familiares=DB::select($queryFamiliares);

                        $i++;
                        $iaux=$i+1;
                        //Manipular Celdas
                        $sheet->cells('A'.$i.':M'.$iaux, function($cells) use ($colorFondoTitular) {

                            //Colorear Fondo
                            $cells->setBackground($colorFondoTitular);

                            // Set font
                            $cells->setFont(array(
                                'family'     => 'Calibri',
                                'size'       => '11',
                                'bold'       =>  true
                            ));

                            //Alineación de Celdas
                            $cells->setAlignment('left');
                        });

                        //colocamos titulos de columnas para el familiar
                        $sheet->cell('B'.$i, function($cell) {$cell->setValue('Familiares:');});

                        $i++;
                        //Manipular Celdas
                        $sheet->cells('C'.$i.':M'.$i, function($cells) {

                            //Colorear Fondo
                            $cells->setBackground('#7FB09C');

                            // Set font
                            $cells->setFont(array(
                                'family'     => 'Calibri',
                                'size'       => '11',
                                'bold'       =>  true
                            ));

                            //Alineación de Celdas
                            $cells->setAlignment('left');
                        });
                        $sheet->cell('C'.$i, function($cell) {$cell->setValue('Nombre');});
                        $sheet->cell('D'.$i, function($cell) {$cell->setValue('Fecha de Nacimiento');});
                        $sheet->cell('E'.$i, function($cell) {$cell->setValue('Sexo');});
                        $sheet->cell('F'.$i, function($cell) {$cell->setValue('Estado');});
                        $sheet->cell('G'.$i, function($cell) {$cell->setValue('Nacionalidad');});
                        $sheet->cell('H'.$i, function($cell) {$cell->setValue('Parentesco');});

                        //se imprimen los datos de familiares

                        foreach ($familiares as $key => $familiar) {
                            $i++;
                            //Manipular Celdas
                            $sheet->cells('A'.$i.':M'.$i, function($cells) use ($colorFondoTitular) {

                                //Colorear Fondo
                                $cells->setBackground($colorFondoTitular);

                                // Set font
                                $cells->setFont(array(
                                    'family'     => 'Calibri',
                                    'size'       => '11',
                                    'bold'       =>  false
                                ));

                                //Alineación de Celdas
                                $cells->setAlignment('left');
                            });

                            $sheet->cell('C'.$i, function($cell) use ($familiar){
                                $cell->setValue($familiar->fullnombrefamiliar);
                            });

                            $sheet->cell('D'.$i, function($cell) use ($familiar){
                                $cell->setValue($familiar->fechaNacimiento);
                            });

                            if($fila->sexo = 1)
                                $sheet->cell('E'.$i, function($cell){
                                    $cell->setValue("M");
                                });
                            else
                                $sheet->cell('E'.$i, function($cell){
                                    $cell->setValue("F");
                                });

                            $sheet->cell('F'.$i, function($cell) use ($familiar){
                                $cell->setValue($familiar->estado);
                            });

                            $sheet->cell('G'.$i, function($cell) use ($familiar){
                                $cell->setValue($familiar->nacionalidad);
                            });

                            $sheet->cell('H'.$i, function($cell) use ($familiar){
                                $cell->setValue($familiar->parentesco);
                            });

                        }
                    }

                    //Se imprimen los contactos del titular
                    $queryContactos= "select concat(upper(contactos.nombre), ' ', upper(contactos.ap_paterno), '  ', upper(contactos.ap_materno)) as fullnombrecontacto, paiscontacto.pais, estadocontacto.estado, concat(contactos.telefono, ' / ', contactos.celular) as telefonocelular, concat(contactos.correo, ' / ', contactos.red_social) as correored, parentesco.descripcion as parentesco from persona inner join viajero on viajero.id_persona = persona.id_persona inner join contactos on viajero.id_viajero = contactos.id_viajero inner join catalogo_det parentesco on parentesco.id_catalogo = 4  and parentesco.id_opcion = contactos.id_parentesco inner join cat_pais paiscontacto on paiscontacto.id_pais = contactos.id_pais inner join cat_estado estadocontacto on estadocontacto.id_estado = contactos.id_estado where persona.id_persona = ".$fila->id_persona;

                    $contactos=DB::select($queryContactos);

                    $i++;
                    $iaux=$i+1;

                    $sheet->cells('A'.$i.':M'.$iaux, function($cells) use ($colorFondoTitular) {

                        //Colorear Fondo
                        $cells->setBackground($colorFondoTitular);

                        // Set font
                        $cells->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '11',
                            'bold'       =>  true
                        ));

                        //Alineación de Celdas
                        $cells->setAlignment('left');
                    });

                    //colocamos titulos de columnas para el contacto
                    $sheet->cell('B'.$i, function($cell) {$cell->setValue('Contactos:');});

                    $i++;
                    //Manipular Celdas
                    $sheet->cells('C'.$i.':M'.$i, function($cells) {

                        //Colorear Fondo
                        $cells->setBackground('#7FB09C');

                        // Set font
                        $cells->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '11',
                            'bold'       =>  true
                        ));

                        //Alineación de Celdas
                        $cells->setAlignment('left');
                    });
                    $sheet->cell('C'.$i, function($cell) {$cell->setValue('Nombre');});
                    $sheet->cell('D'.$i, function($cell) {$cell->setValue('País');});
                    $sheet->cell('E'.$i, function($cell) {$cell->setValue('Estado');});
                    $sheet->cell('F'.$i, function($cell) {$cell->setValue('Teléfono');});
                    $sheet->cell('G'.$i, function($cell) {$cell->setValue('Medio de Contacto');});
                    $sheet->cell('H'.$i, function($cell) {$cell->setValue('Parentesco');});

                    //se imprimen los datos de contacto
                    foreach ($contactos as $key => $contacto) {
                        $i++;
                        //Manipular Celdas
                        $sheet->cells('A'.$i.':M'.$i, function($cells) use ($colorFondoTitular) {

                            //Colorear Fondo
                            $cells->setBackground($colorFondoTitular);

                            // Set font
                            $cells->setFont(array(
                                'family'     => 'Calibri',
                                'size'       => '11',
                                'bold'       =>  false
                            ));

                            //Alineación de Celdas
                            $cells->setAlignment('left');
                        });

                        $sheet->cell('C'.$i, function($cell) use ($contacto){
                            $cell->setValue($contacto->fullnombrecontacto);
                        });

                        $sheet->cell('D'.$i, function($cell) use ($contacto){
                            $cell->setValue($contacto->pais);
                        });

                        $sheet->cell('E'.$i, function($cell) use ($contacto){
                            $cell->setValue($contacto->estado);
                        });

                        $sheet->cell('F'.$i, function($cell) use ($contacto){
                            $cell->setValue($contacto->telefonocelular);
                        });

                        $sheet->cell('G'.$i, function($cell) use ($contacto){
                            $cell->setValue($contacto->correored);
                        });

                        $sheet->cell('H'.$i, function($cell) use ($contacto){
                            $cell->setValue($contacto->parentesco);
                        });
                    }

                    //se deja espacio entre titulares
                    $i++;
				}


            });


        })->save('xlsx', public_path("/reporte"));

        $filename.='.xlsx';
        return $filename;
    }

}
