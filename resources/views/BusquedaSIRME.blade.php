<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
 ?>
@extends('layouts.app')

@section('content')

	@include('layouts.menu')

	<div class="row">
		<div class="col-md-12 loginSize">
			<H1>Correo electrónico masivo SIRME</H1>
			<hr class="red"/>
		</div>
	</div>

	<div class="form-group">
            <div class="row">
                <div class="form-group datepicker-group col-md-6  ">
                    <label class="control-label" for="calendar">Fecha de Listado*:</label>
                    <input class="form-control" id="datepicker" type="text" onchange="seleccionarFecha()" readonly>
                    <span class="glyphicon glyphicon-calendar alinearCalendario" aria-hidden="true"></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label" for="cbPais">Seleccione los criterios de búsqueda:</label>
        </div>
            <div class="form-group">
                <div class="row">
                    <div class="form-group col-md-6">

                        <label class="control-label" for="pais">País*:</label>
                    <select class="form-control @if($errors->has('pais')) bordeErrorLogin  @endif" id="Idpais" name="Idpais" onchange="cargarComboEstado()">
                        <option value="0" selected="true">Seleccione un País</option>
                        @foreach($Pais as $pais)
                            <option value="{{$pais->id_pais}}">{{$pais->pais}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('pais'))
                        @foreach($errors->get('pais') as $error)
                            <div class="text-muted textErrorLogin">{{$error}}</div>
                        @endforeach
                    @endif


                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="control-label" for="cbEstado">Estado:</label>
                        <select class="form-control" id="Idestado" name="Idestado" onchange="cargarComboCondado()">
                            <option value="0" selected="true"">Seleccione primero País</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="control-label" for="cbCondado">Condado:</label>
                        <select class="form-control" id="Idcondado" name="Idcondado" onchange="seleccionarCondado()" disabled="disabled">
                            <option value="0" selected="true"">Seleccione primero Estado</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6" hidden="hidden">
                        <label class="control-label" id="IdCase"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <div class="pull-left text-muted"> * Campos Obligatorios.</div>
                    </div>
                </div>
            </div>
        <!--Controles de Busqueda-->
            <div class="row">
                <div class="form-group col-md-12 text-right">
                    <input id="btnLimpiar" type="button" class="btn btn-default" value="Limpiar" onclick="limpiarCampos()">
                    <input id="btnBuscarMex" type="button" class="btn btn-primary" value="Generar" onclick="BuscarMexicanos()">
                </div>
                <div class=" form-group col-md-4">

                </div>
                <hr />
            </div>


            <!--Resultados-->
    <section id="seccionResult" hidden="hidden">
        <div class="row">
            <div class="col-md-12">
                <h2 id="CadenaResultado"></h2>
                <hr class="red" />

            </div>
        </div>


        <div class="row">
            <div class="form-group col-md-1">
            </div>
            <div class="form-group col-md-3">
                <canvas id="pie-chart" class="graficoAlinear"></canvas>
            </div>
            <div class="form-group col-md-1">
            </div>
            <div class="form-group col-md-2">
                <img src="/images/personas.jpg" id="imagenpersonas" aling="center" width="150" class="imagenesGraficos" />
                <H6 size="4" align="center" ><b>Total de Registros</b></H6>
                <h2 class="resultGraficosText" id="TotalRegistros"></h2>
            </div>
            <div class="form-group col-md-2">
                <img src="/images/viajero.jpg" aling="center" width="150" class="imagenesGraficos" />
                <H6 size="4" align="center" ><b>Viajeros</b></H6>
                <h2 id="TotalViajeros" class="resultGraficosText">123</h2>
            </div>
            <div class="form-group col-md-2">
                <img src="/images/home.png" aling="center" width="150" class="imagenesGraficos" />
                <H6 size="4" align="center" ><b>Residentes</b></H6>
                <h2 id="TotalResidentes" class="resultGraficosText">123</h2>
            </div>
            <div class="form-group col-md-1">
            </div>
        </div>

        <div class="form-group">

            <div class="row">
                <div class="col-md-12 text-right">
                    <input id="ExportarExcel" type="button" class="btn btn-primary imgexcel" value="Exportar .XLS" onclick="ExportarExcel()"/>
                    <input id="ExportarWord" type="button" class="btn btn-primary imgword" value="Exportar .DOC" onclick="ExportarWord()"/>
                    <hr class="sinLinea" />
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="ModalResult" name="ModalResult">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Resumen Consulta SIRME</h4>
                </div>
                <div class="modal-body">
                    <p id="MensajeResultados"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModalLoading" name="ModalLoading" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="loader">
                <span class="loader-block"></span>
                <span class="loader-block"></span>
                <span class="loader-block"></span>
                <span class="loader-block"></span>
                <span class="loader-block"></span>
                <span class="loader-block"></span>
                <span class="loader-block"></span>
                <span class="loader-block"></span>
                <span class="loader-block"></span>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function (){
            window.scrollTo(0, 0);
            $("#datepicker").datepicker();
            document.getElementById("Idpais").selectedIndex = "0";
            document.getElementById("datepicker").value = "";
            document.getElementById("btnBuscarMex").disabled = true;


            var config = {
                type: 'pie',
                data: {

                    display: true,
                    datasets: [{

                        backgroundColor: ["#3483de", "#8e5ea2"],
                        data: [0, 0]
                    }],
                    labels: ["VIAJEROS", "RESIDENTES"]
                },

                options: {
                    title: {
                        display: false,
                        text: 'MEXICANOS EN EL EXTERIOR'
                    },
                    legend: {
                        display: false,
                    }
                }
            };

            var ctx = document.getElementById("pie-chart").getContext("2d");
            window.myPie = new Chart(ctx, config);
        });

				function ExportarWord(){
					$('#ModalLoading').modal('show');
						$.ajax({
												Type: 'POST',
												url: '/ExportarWord'
										})
						.done(function(file_name){
								var link = document.createElement('a');
				        link.href = "/DescargaArchivo/"+file_name;
				        document.body.appendChild(link);
								$('#ModalLoading').modal('hide');
				        link.click();

						})
						.fail(function(){
							var link = document.createElement('a');
							link.href = "/";
							document.body.appendChild(link);
							link.click();
						})
				}

				function ExportarExcel(){
					$('#ModalLoading').modal('show');
						$.ajax({
												Type: 'POST',
												url: '/ExportarExcel'
										})
						.done(function(file_name){
							var link = document.createElement('a');
							link.href = "/DescargaArchivo/"+file_name;
							document.body.appendChild(link);
							$('#ModalLoading').modal('hide');
							link.click();
						})
						.fail(function(){
							var link = document.createElement('a');
							link.href = "/";
							document.body.appendChild(link);
							link.click();
						})
				}

        function seleccionarFecha(){
            if ($("#Idpais").val() > 0) {
                document.getElementById("btnBuscarMex").disabled = false;
            }
        }

        function cargarComboEstado(){
            if($("#Idpais option:selected").val()>0){
                $("#Idestado").empty();

                document.getElementById("Idpais").disabled  = true;
                document.getElementById("Idestado").disabled  = false;
                document.getElementById("Idcondado").disabled = true;

                if ($("#datepicker").val() != "") {
                    document.getElementById("btnBuscarMex").disabled = false;
                }

                //Asignamos case 1
                document.getElementById("IdCase").value = 1;

                $.ajax({
                            Type: 'POST',
                            url: '/ObtenerEstado',
                            data: {'idPais' :  $("#Idpais option:selected").val()}
                        })
                .done(function(listaEstados){
                    $('#Idestado').html(listaEstados);
                })
                .fail(function(){
                })
            }
        }

        function cargarComboCondado(){
            if($("#Idpais").val()>0){
                //Asignamos el caso 2 para la busqueda por condado
                document.getElementById("IdCase").value = 2;
            }

                if($("#Idpais").val()==50){
                    $("#Idcondado").empty();

                    document.getElementById("Idpais").disabled = true;
                    document.getElementById("Idestado").disabled = true;
                    document.getElementById("Idcondado").disabled = false;

                    $.ajax({
                                Type: 'POST',
                                url: '/ObtenerCondado',
                                data: {'idEstado' :  $("#Idestado option:selected").val()}
                            })
                    .done(function(listaCondados){
                        $('#Idcondado').html(listaCondados);
                    })
                    .fail(function(){
                    })
                }
        }

        function seleccionarCondado(){
            if($("#Idpais").val()>0){
                //Asignamos el caso 2 para la busqueda por condado
                document.getElementById("IdCase").value = 3;
            }
        }

        function limpiarCampos() {

            $("#Idcondado").empty();
            $("#Idestado").empty();
            $("#Idestado").append('<option value="' + 0 + '">' + "Seleccione primero País" + '</option>');
            $("#Idcondado").append('<option value="' + 0 + '">' + "Seleccione primero Estado" + '</option>');
            document.getElementById("Idpais").selectedIndex = "0";

            document.getElementById("Idpais").disabled = false;
            document.getElementById("Idestado").disabled = false;
            document.getElementById("Idcondado").disabled = true;
            document.getElementById("seccionResult").hidden = true;


            document.getElementById("datepicker").value = "";
            window.scrollTo(0, 0);
        }

        function BuscarMexicanos() {
            $idCase = $("#IdCase").val();
            if($("#IdCase").val()==1){
                $valorPais = $("#Idpais").val();
                $valorEstado = 0;
                $valorCondado = 0;
                $valorFecha = $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'}).val();
                $cadenaResultado = $("#Idpais option:selected").text();
            }

            if($("#IdCase").val()==2){
                $valorPais = $("#Idpais").val();
                $valorEstado = $("#Idestado").val();
                $valorCondado = 0;
                $valorFecha = $("#datepicker").val();
                $cadenaResultado = $("#Idestado option:selected").text() + ' ,' + $("#Idpais option:selected").text();
            }

            if($("#IdCase").val()==3){
                $valorPais = $("#Idpais").val();
                $valorEstado = $("#Idestado").val();
                $valorCondado = $("#Idcondado").val();;
                $valorFecha = $("#datepicker").val();
                $cadenaResultado = $("#Idcondado option:selected").text() + ', ' + $("#Idestado option:selected").text() + ' ,' + $("#Idpais option:selected").text();
            }

            $('#ModalLoading').modal('show');
            document.getElementById("seccionResult").hidden = false;
            document.getElementById("btnBuscarMex").disabled = true;
            $.ajax({
                Type: 'POST',
                url: '/ObtenerTotalRegistros',
                data: {
                    'idCase'        : $idCase,
                    'valorPais'     : $valorPais,
                    'valorEstado'   : $valorEstado,
                    'valorCondado'  : $valorCondado,
                    'valorFecha'    : $valorFecha
                    }
                })
                .done(function (TotalTitulares) {

                    $('#ModalLoading').modal('hide');
                    $('#ModalResult').modal('show');
                    $('#MensajeResultados').text("Se encontraron connacionales en el extranjero con los filtros seleccionados.");

                    $.ajax({
                        Type: 'POST',
                        url: '/ObtenerTotalViajeros',
                            data: {},
                            success: function (TotalViajeros) {

                                var datos = [TotalViajeros, (TotalTitulares - TotalViajeros)];

                                window.myPie.data.datasets[0].data = datos;
                                window.myPie.update();

                                $('#TotalRegistros').text(TotalTitulares);
                                $('#CadenaResultado').text("RESULTADO FILTRO(S): " + $cadenaResultado);
                                $('#TotalViajeros').text(TotalViajeros);
                                $('#TotalResidentes').text((TotalTitulares - TotalViajeros));
                            }
                    });

                        document.getElementById("btnBuscarMex").disabled = true;
                        window.scrollTo(0, 500);

                        $("#Idcondado").empty();
                        $("#Idestado").empty();
                        document.getElementById("Idpais").disabled = false;
                        document.getElementById("Idestado").disabled = true;
                        document.getElementById("Idcondado").disabled = true;
                        document.getElementById("btnBuscarMex").disabled = true;
                        document.getElementById("datepicker").value = "";
                        document.getElementById("Idpais").selectedIndex = "0";
                        document.getElementById("IdCase").value = 0;
                        $("#Idestado").append('<option value="' + 0 + '">' + "Seleccione primero País" + '</option>');
                        $("#Idcondado").append('<option value="' + 0 + '">' + "Seleccione primero Estado" + '</option>');


                })
                .fail(function(){
                })
        }



    </script>


@endsection

@section('title')
	SIRME - Búsqueda
@endsection
