<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
 ?>
@extends('layouts.app')

@section('content')
    <div class="row loginSize">
        <div class="col-md-5 ">
            <div align="center">
                <img src="{{secure_asset('images/sirme.jpg')}}" width="400px" height="500px">
            </div>

        </div>
        <div class="col-md-6">
            <form method="POST" action="/loginValidate">
                {{ csrf_field() }}
                <font size="30"><b>SIRME</b></font><font size="1">V 1.1</font>
                <div class="form-group">
                    <label class="control-label" for="user">Usuario*:</label>
                    <input class="form-control @if($errors->has('user')) bordeErrorLogin  @endif" id="user" name="user" placeholder="Usuario Active Directory" type="text" value="{{ old('user') }}">
                    @if ($errors->has('user'))
                        @foreach($errors->get('user') as $error)
                            <div class="text-muted textErrorLogin">{{$error}}</div>
                        @endforeach
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label" for="pwd">Contraseña*:</label>
                    <input class="form-control @if($errors->has('pwd')) bordeErrorLogin  @endif" id="pwd" name="pwd" placeholder="Contraseña Active Directory" type="password" value="{{ old('pwd') }}">
                    @if ($errors->has('pwd'))
                        @foreach($errors->get('pwd') as $error)
                            <div class="text-muted textErrorLogin">{{$error}}</div>
                        @endforeach
                    @endif
                </div>

                <div class="">
                    <div class="pull-left text-muted">
                        * Campos Obligatorios
                    </div>
                </div>
                <div class="text-right">
                    <input type="submit" name="login" id="login" class="btn btn-primary" value="Iniciar Sesión">
                </div>

                @if(Session::has('ErrorLogin'))
                    <div class="text-muted textErrorLogin">
                        {{Session::get('ErrorLogin')}}
                    </div>
                @endif

            </form>
        </div>
        <div class="col-md-1">
        </div>
    </div>

@endsection

@section('title')
    SIRME - Reporte
@endsection
